package src;
/////comentario de prueba
public class Televisor {
    int canalMinimo = 1;
    int canalMaximo = 10;
    int canalActual = 0;

    int volumenMinimo = 0;
    int volumenMaximo = 100;
    int volumenActual = 10;

    boolean mute = false;

    boolean encendido = false;
    String entradas[] = {"HDMI", "Audio y Video", "TV"};
    String entradaActual;

    public Televisor(int entrada) {
        System.out.println("Señal actual: " + this.entradas[entrada - 1]);
        this.entradaActual = this.entradas[entrada - 1];
    }

    public int getCanalActual() {
        return canalActual;
    }

    public void setCanalActual(int canalActual) {
        if (this.entradaActual != this.entradas[2]) {
            System.out.println("Solo se puden cambiar canales en señal TV");
            return;
        }
        if (canalActual < canalMinimo) {
            this.canalActual = canalMaximo;
        } else if (canalActual > canalMaximo) {
            this.canalActual = canalMinimo;
        } else {
            this.canalActual = canalActual;
        }
    }

    public int getCanalMinimo() {
        return canalMinimo;
    }

    public void setCanalMinimo(int canalMinimo) {
        this.canalMinimo = canalMinimo;
    }

    public int getCanalMaximo() {
        return canalMaximo;
    }

    public void setCanalMaximo(int canalMaximo) {
        this.canalMaximo = canalMaximo;
    }

    public int getVolumenMinimo() {
        return volumenMinimo;
    }

    public void setVolumenMinimo(int volumenMinimo) {
        this.volumenMinimo = volumenMinimo;
    }

    public int getVolumenMaximo() {
        return volumenMaximo;
    }

    public void setVolumenMaximo(int volumenMaximo) {
        this.volumenMaximo = volumenMaximo;
    }

    public int getVolumenActual() {
        return volumenActual;
    }

    public void setVolumenActual(int volumenActual) {
        if (volumenActual <= this.volumenMinimo) {
            this.volumenActual = this.volumenMinimo;
        } else if (volumenActual >= this.volumenMaximo) {
            this.volumenActual = this.volumenMaximo;
        } else {
            this.volumenActual = volumenActual;
        }
    }

    public void doMute() {
        this.mute = !this.mute;
    }
}
