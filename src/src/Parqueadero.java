package src;

public class Parqueadero {

    int limite = 5;
    int limiteMoto = 2;
    int limiteBicicleta = 3;

    Vehiculo[][] espacios = new Vehiculo[limite][3];

    public Parqueadero() {
        //System.out.println(this.espacios.length);
    }

    public void agregarAutomovil(Vehiculo vehiculo, int espacio) {
        if (espacios[espacio][0] != null) {
            System.out.println("El espacio se encuentra ocupado");
            System.out.println("***************");
            return;
        }
        espacios[espacio][0] = vehiculo;
        espacios[espacio][1] = vehiculo;
        espacios[espacio][2] = vehiculo;
    }

    public void agregarBicicleta(Vehiculo vehiculo, int espacio, int cupo) {
        if (espacios[espacio][cupo] != null) {
            System.out.println("El espacio se encuentra ocupado");
            System.out.println("***************");
            return;
        }
        int count = 0;
        for (int j = 0; j < this.espacios[espacio].length; j++) {
            if (this.espacios[espacio][j] != null) {
                count++;
                if (this.espacios[espacio][j].tipo != vehiculo.tipo) {
                    System.out.println("En el espacio N° " + (espacio + 1) + " solo puede ingresar vehiculos de tipo " + this.espacios[espacio][j].tipo);
                    return;
                }
            }
        }

        if (count == limiteBicicleta) {
            System.out.println("No se pueden agregar mas bicicletas");
            System.out.println("***************");
            return;
        }

        espacios[espacio][cupo] = vehiculo;
    }

    public void agregarMoto(Vehiculo vehiculo, int espacio, int cupo) {
        if (espacios[espacio][cupo] != null) {
            System.out.println("El espacio se encuentra ocupado");
            System.out.println("***************");
            return;
        }
        int count = 0;
        for (int j = 0; j < this.espacios[espacio].length; j++) {
            if (this.espacios[espacio][j] != null) {
                count++;
                if (this.espacios[espacio][j].tipo != vehiculo.tipo) {
                    System.out.println("En el espacio N° " + (espacio + 1) + " solo puede ingresar vehiculos de tipo " + this.espacios[espacio][j].tipo);
                    return;
                }
            }
        }

        if (count == limiteMoto) {
            System.out.println("No se pueden agregar mas motos");
            System.out.println("***************");
            return;
        }
        espacios[espacio][cupo] = vehiculo;
    }

    public void verificarCupos() {
        for (int i = 0; i < this.espacios.length; i++) {
            if (this.espacios[i] == null) {
                System.out.println("Cupo " + (i + 1) + " disponible:");
            } else {
                for (int j = 0; j < this.espacios[i].length; j++) {

                    if (this.espacios[i][j] == null) {
                        System.out.println("parqueadero N° " + (i + 1) + "-" + (j + 1) + " disponible");
                    } else {
                        System.out.println("parquedero N° " + (i + 1) + "-" + (j + 1) + " Ocupado por: " + espacios[i][j].tipo + " - " + espacios[i][j].matricula);
                    }
                }
            }
        }
    }

}
