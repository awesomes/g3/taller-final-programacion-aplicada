package src;

public class Vehiculo {

    String tipo;
    String matricula;

    public Vehiculo() {
    }

    public Vehiculo(String tipo, String matricula) {
        this.tipo = tipo;
        this.matricula = matricula;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }
}
