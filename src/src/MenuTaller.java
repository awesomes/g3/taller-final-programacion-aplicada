package src;

import java.util.Arrays;
import java.util.Scanner;

public class MenuTaller {

    public static void main(String[] args) {
        //comentario de prueba commit
        Scanner scanner = new Scanner(System.in);
        boolean continuar = true;
        while (continuar) {
            System.out.println("Seleccione una opción");
            System.out.println("1. Tele");
            System.out.println("2. Estacionamiento");
            System.out.println("3. Lotería");
            System.out.println("4. Taller 1");
            System.out.println("0. Salir");
            int input = scanner.nextInt();
            switch (input) {
                case 0:
                    System.out.println("Menu 0 termina");
                    continuar = false;
                    break;
                case 1:
                    // Programa Television
                    boolean opcionInvalida = true;
                    int opcion = 3;
                    while (opcionInvalida) {
                        System.out.println("Menu 1 tele");
                        System.out.println("1. HDMI");
                        System.out.println("2. Audio y Video");
                        System.out.println("3. TV");
                        opcion = scanner.nextInt();
                        if (opcion == 1 || opcion == 2 || opcion == 3) {
                            opcionInvalida = false;
                        }
                    }

                    int entradaSenal = opcion;
                    Televisor televisor = new Televisor(entradaSenal);
                    //encendido del televisor
                    televisor.encendido = true;
                    System.out.println("Televisor encendido");

                    //Si selecciona HDMI o Audio y Video.
                    if (opcion == 1 || opcion == 2) {
                        int volumen;

                        //variable para romper el ciclo while que me arroja constantemente el menu
                        boolean aceptar = false;

                        while (!aceptar) {
                            volumen = televisor.getVolumenActual();
                            System.out.println("Oprima 1 para subir volumen y 2 pra bajar.");
                            System.out.println("Oprima 4 para activar/desactivar el mute");
                            System.out.println("Oprima 0 para apagar");
                            int canalOpcion = scanner.nextInt();

                            //Aumenta el valor de la variable del volumen y quita el mute
                            if (canalOpcion == 1) {
                                televisor.mute = false;
                                volumen++;

                                //Disminuye el valor de la variable del volumen y quita el mute
                            } else if (canalOpcion == 2) {
                                televisor.mute = false;
                                volumen--;

                                //Llama el metodo que alterna la variable del mute en true o false segun corresponda
                            } else if (canalOpcion == 4) {
                                televisor.doMute();

                                //Cambia la variable de apagado a true
                            } else if (canalOpcion == 0) {
                                televisor.encendido = false;
                                aceptar = true;
                                System.out.println("Televisor apagado");
                            }
                            televisor.setVolumenActual(volumen);
                            mensajeVolumen(televisor, (opcion == 1) ? "HDMI" : "Audio y Video");
                        }
                    }

                    if (opcion == 3) {
                        int canal;
                        int volumen;
                        boolean aceptar = false;
                        while (!aceptar) {

                            //obtiene los valores de canal y volumen por defecto o los se hayan seleccionado previamente
                            canal = televisor.getCanalActual();
                            volumen = televisor.getVolumenActual();

                            System.out.println("*******************************************");
                            System.out.println("*******************************************");
                            System.out.println("Oprima 8 para aumentar de canal y 5 para disminuir.");
                            System.out.println("Oprima 3 para seleccionar manualmente el canal.");
                            System.out.println("Oprima 1 para subir volumen y 2 pra bajar.");
                            System.out.println("Oprima 4 para activar/desactivar el mute");
                            System.out.println("Oprima 0 para apagar");

                            //lectura de consola
                            int canalOpcion = scanner.nextInt();

                            //Aumenta el valor de la variable del canal
                            if (canalOpcion == 8) {
                                canal++;

                                //Disminuye el valor de la variable del canal
                            } else if (canalOpcion == 5) {
                                canal--;

                                //Opcion para digitar canal directamente
                            } else if (canalOpcion == 3) {
                                System.out.println("Digite número de canal " + televisor.getCanalMinimo() + " - " + televisor.getCanalMaximo());
                                canalOpcion = scanner.nextInt();
                                canal = canalOpcion;

                                //Aumenta el valor de la variable del volumen y quita el mute
                            } else if (canalOpcion == 1) {
                                televisor.mute = false;
                                volumen++;

                                //Disminuye el valor de la variable del volumen y quita el mute
                            } else if (canalOpcion == 2) {
                                televisor.mute = false;
                                volumen--;

                                //Llama el metodo que alterna la variable del mute en true o false segun corresponda
                            } else if (canalOpcion == 4) {
                                televisor.doMute();

                                //Cambia la variable de apagado a true
                            } else if (canalOpcion == 0) {
                                televisor.encendido = false;
                                aceptar = true;
                                System.out.println("Televisor apagado");
                            }
                            // Cambio de valores de volumen y canal segun las selecciones anteriores
                            televisor.setVolumenActual(volumen);
                            televisor.setCanalActual(canal);
                            mensajeVolumen(televisor, "");
                        }
                    }

                    break;
                case 2:
                    //Programa parqueadero
                    boolean salir = false;
                    Parqueadero parqueadero = new Parqueadero();
                    while (!salir) {
                        System.out.println("***************");
                        System.out.println("***************");
                        System.out.println("Menu 1 Parqueadero");
                        System.out.println("Oprima 1 Para agregar un Automovil.");
                        System.out.println("Oprima 2 Para agregar una Bicicleta.");
                        System.out.println("Oprima 3 Para agregar una Moto.");
                        System.out.println("Oprima 0 Para salir.");

                        int opcionVehiculo = scanner.nextInt();
                        Vehiculo vehiculo = new Vehiculo();
                        int numparqueadero;
                        String matr;
                        int numeroCupo;

                        switch (opcionVehiculo) {
                            case 0:
                                salir = true;
                                break;

                            case 1:

                                vehiculo.tipo = "Automovil";
                                System.out.println("Digite la matricula");
                                matr = scanner.next();
                                vehiculo.matricula = matr;
                                parqueadero.verificarCupos();
                                System.out.println("Ingrese el parqueadero a ocupar");
                                numparqueadero = scanner.nextInt();
                                parqueadero.agregarAutomovil(vehiculo, (numparqueadero - 1));
                                parqueadero.verificarCupos();
                                break;

                            case 2:

                                vehiculo.tipo = "Bicicleta";
                                System.out.println("Digite la matricula");
                                matr = scanner.next();
                                vehiculo.matricula = matr;
                                parqueadero.verificarCupos();
                                System.out.println("Ingrese el parqueadero a ocupar");
                                numparqueadero = scanner.nextInt();
                                System.out.println("Ingrese el cupo");
                                numeroCupo = scanner.nextInt();
                                parqueadero.agregarBicicleta(vehiculo, (numparqueadero - 1), (numeroCupo - 1));
                                parqueadero.verificarCupos();
                                break;

                            case 3:
                                vehiculo.tipo = "Moto";
                                System.out.println("Digite la matricula");
                                matr = scanner.next();
                                vehiculo.matricula = matr;
                                parqueadero.verificarCupos();
                                System.out.println("Ingrese el parqueadero a ocupar");
                                numparqueadero = scanner.nextInt();
                                System.out.println("Ingrese el cupo");
                                numeroCupo = scanner.nextInt();
                                parqueadero.agregarMoto(vehiculo, (numparqueadero - 1), (numeroCupo - 1));
                                parqueadero.verificarCupos();
                                break;

                            default:
                                System.out.println("Opción no válida");
                                break;
                        }

                    }
                    break;

                case 3:
                    // Programa Loteria
                    Loteria loteria = new Loteria();
                    System.out.println("Ingrese el primer numero de loteria: ");
                    int num1 = scanner.nextInt();
                    System.out.println("Ingrese el segundo numero de loteria: ");
                    int num2 = scanner.nextInt();
                    System.out.println("Ingrese el tercer numero de loteria: ");
                    int num3 = scanner.nextInt();
                    int ganador = loteria.jugar();
                    System.out.println("El numero de loteria es: " + ganador);
                    if (num1 == ganador || num2 == ganador || num3 == ganador) {
                        System.out.println("Has ganado el premio");
                    }
                    else {
                        System.out.println("Desafortunadamente has perdido");
                    }
                    break;
                case 4:
                    //Programa primer taller
                    boolean continuar2 = true;
                    while (continuar2) {

                        System.out.println("************************");
                        System.out.println("1. Ejercicios de Agilidad");
                        System.out.println("2. Ejercicios de Conversión");
                        System.out.println("3. Ejercicios de Geometría");
                        System.out.println("0. Regresar");
                        System.out.println("Escribe una de las opciones");
                        System.out.println("************************");
                        int opcion2 = scanner.nextInt();

                        switch (opcion2) {

                            case 0:
                                System.out.println("Fin del Programa");
                                continuar2 = false;
                                break;

                            case 1:
                                Scanner scanner2 = new Scanner(System.in);
                                boolean continuar3 = true;
                                while (continuar3) {
                                    System.out.println("************************");
                                    System.out.println("************************");
                                    System.out.println("1. Numero mayor que");
                                    System.out.println("2. Ordenar de menor a mayor");
                                    System.out.println("3. Número más pequeño del array");
                                    System.out.println("0. Regresar");
                                    System.out.println("Escribe una de las opciones");
                                    int opcion3 = scanner.nextInt();

                                    switch (opcion3) {

                                        case 0:
                                            System.out.println("Menu 0 termina");
                                            continuar3 = false;
                                            break;

                                        case 1:
                                            String numA, numB;
                                            Agility ejerciciosTaller = new Agility();
                                            System.out.println("Ingrese por favor el primer numero");
                                            numA = scanner.next();
                                            System.out.println("Ingrese por favor el segundo numero");
                                            numB = scanner.next();
                                            boolean resultado = ejerciciosTaller.biggerThan(numA, numB);
                                            if (resultado) {
                                                System.out.println("El primer numero es mayor que el segundo");
                                            } else {
                                                System.out.println("El segundo numero es mayor que el primero");
                                            }
                                            break;


                                        case 2:
                                            Agility ejerciciosTaller1 = new Agility();
                                            int numero1, numero2, numC, numD, numE;
                                            numero1 = 0;
                                            numero2 = 0;
                                            System.out.println("Ingrese el Numero 1: ");
                                            num1 = scanner.nextInt();
                                            System.out.println("Ingrese el Numero 2: ");
                                            num2 = scanner.nextInt();
                                            System.out.println("Ingrese el Numero 3: ");
                                            numC = scanner.nextInt();
                                            System.out.println("Ingrese el Numero 4: ");
                                            numD = scanner.nextInt();
                                            System.out.println("Ingrese el Numero 5: ");
                                            numE = scanner.nextInt();
                                            int resultado1[] = ejerciciosTaller1.order(numero1, numero2, numC, numD, numE);
                                            System.out.println(Arrays.toString(resultado1));
                                            break;
                                    }
                                }
                                break;

                            case 2:
                                System.out.println("Prueba 2");
                                break;

                            case 3:
                                Scanner scanner3 = new Scanner(System.in);
                                boolean continuar4 = true;
                                while (continuar4) {

                                    System.out.println("************************");
                                    System.out.println("1. Area del cuadrado");
                                    System.out.println("2. Area del cuadrado dos lados");
                                    System.out.println("3. Radio del circulo");
                                    System.out.println("4. Perimetro del Cuadrado");
                                    System.out.println("5. Volumen de la Esfera");
                                    System.out.println("6. Area Pentagono");
                                    System.out.println("7. Calcular la Hipotenusa");
                                    System.out.println("0. Regresar");
                                    System.out.println("Escribe una de las opciones");
                                    int opcion3 = scanner.nextInt();

                                    switch (opcion3) {

                                        case 0:
                                            System.out.println("Menu 0 termina");
                                            continuar4 = false;
                                            break;

                                        case 1:
                                            int side;
                                            Geometric areaCuadrado = new Geometric();
                                            System.out.println("Ingrese el valor del lado");
                                            side = scanner.nextInt();
                                            int area = 0;
                                            System.out.println("El área del Cuadrado es: " + side * side);
                                            break;

                                        case 2:
                                            int sidea, sideb;
                                            Geometric areaCuadradoDosLados = new Geometric();
                                            System.out.println("Ingrese el valor del lado A");
                                            sidea = scanner.nextInt();
                                            System.out.println("Ingrese el valor del lado B");
                                            sideb = scanner.nextInt();
                                            int area1 = 0;
                                            System.out.println("El área del Cuadrado es: " + sidea * sideb);
                                            break;

                                        case 3:
                                            double radius;
                                            Geometric radioCirculo = new Geometric();
                                            System.out.println("Ingrese el valor del radio");
                                            radius = scanner.nextDouble();
                                            System.out.println("El radio del Circulo es: " + Math.PI * 2 * radius);
                                            break;

                                        case 4:
                                            //perimetro cuadrado
                                            double sideC;
                                            Geometric perimetroCuadrado = new Geometric();
                                            System.out.println("Ingrese el valor del lado");
                                            sideC = scanner.nextDouble();
                                            System.out.println("El perimetro del Cuadrado es: " + (sideC + sideC + sideC + sideC));
                                            break;

                                        case 5:
                                            double v = (4.0 / 3), radius2;
                                            Geometric volumenEsfera = new Geometric();
                                            System.out.println("Ingrese el valor del radio");
                                            radius2 = scanner.nextDouble();
                                            System.out.println("El volumen de la esfera es " + (v * Math.PI * radius2 * radius2 * radius2));
                                            break;

                                        case 6:
                                            double side3;
                                            Geometric areaPentagono = new Geometric();
                                            System.out.println("Ingrese el valor del Lado");
                                            side3 = scanner.nextDouble();
                                            System.out.println("El valor del area del pentagono es: " + (Math.sqrt(5 * (5 + 2 * (Math.sqrt(5)))) * side3 * side3) / 4);
                                            break;

                                        case 7:
                                            double catA, catB;
                                            Geometric calculaHipotenusa = new Geometric();
                                            System.out.println("Ingrese el valor del Cateto A");
                                            catA = scanner.nextDouble();
                                            System.out.println("Ingrese el valor del Cateto B");
                                            catB = scanner.nextDouble();
                                            System.out.println("El valor de la Hipotenusa es: " + (Math.sqrt((catA * catA + catB * catB))));
                                            break;

                                    }
                                }
                                break;


                        }


                    }
                    break;

            }
        }

    }

    private static void mensajeVolumen(Televisor televisor, String senal) {
        if (televisor.volumenActual == televisor.volumenMaximo) {
            System.out.println("Alcanzó el volumen máximo");
        }
        if (televisor.volumenActual == televisor.volumenMinimo) {
            System.out.println("Alcanzó el volumen mínimo");
        }
        if (senal == "") {
            System.out.println("Canal actual " + televisor.getCanalActual());
        } else {
            System.out.println("Señal actual " + senal);
        }

        System.out.println("Volumen " + televisor.getVolumenActual());
        System.out.println("estado mute: " + televisor.mute);
    }
}
